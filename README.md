# Collatz Service

Collatz es un servicio web REST desarrollado en C++11 que permite calcular la secuencia Collatz de un número natural.

## Instrucciones de Instalación y compilación
Para compilar el proyecto asegúrese de tener acceso a internet y de tener instalado los siguientes componentes:

* g++ (versión 5 o superior)
* Make

Luego dirígase a la **carpeta del proyecto** `collatz` y ejecute el comando  `make install`. Esto instalará las dependencias necesarias para compilar la aplicación.

## Instrucciones de ejecución
Para iniciar el servidor de la aplicación ejecute el comando `make run`

## Testing
Para arrancar los test del servicio ejecute el comando `make test`

## Uso del servicio

A continuación se dan los detalles técnicos para consumir el servicio.

* Método HTTP: GET
* URL de ejemplo: `http://localhost:8080/Collatz/getSecuence?value=123456 ` (puede cambiar el valor 123456 por el que se desee)
* Headers `Content-Type: application/json`

## Contacto
* Jaime Araya Jerez
* Correo: jaimearayajerez@gmail.com
* Celular: +56 9 3060 9938
