#ifndef COLLATZ_DELEGATE_H
#define COLLATZ_DELEGATE_H

#include <list>

class CollatzDelegate {

public:
    /**
     * @brief      Devuelve la secuencia de Collatz dado un número natural.
     *
     * @param[in]  natural  Número natural.
     *
     * @return     Lista con la secuencia de Collatz.
     */
    std::list<long> getSecuence(const long natural) const;
};

#endif
