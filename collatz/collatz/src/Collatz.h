#ifndef COLLATZ_H
#define COLLATZ_H

#include <ngrest/common/Service.h>
#include <list>

/**
 * @brief      Controlador API para calcular la secuencia de Collatz.
 */
class Collatz: public ngrest::Service {

public:
    /**
     * @brief      Devuelve la secuencia de Collatz dado un número natural.
     *
     * @param[in]  value  Número natural.
     *
     * @return     Lista con la secuencia de Collatz.
     */
    std::list<long> getSecuence(const long value) const;
};

#endif
