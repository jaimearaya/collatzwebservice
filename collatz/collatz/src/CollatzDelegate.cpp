#include <sstream>
#include "CollatzDelegate.h"

std::list<long> CollatzDelegate::getSecuence(const long natural) const {
	if(natural < 1L) {
		std::stringstream errorMessage;
		errorMessage << natural << " no es un entero positivo.";
		throw std::logic_error(errorMessage.str());
	}

	std::list<long> collatzList;

	for(long value = natural; value != 1; value = value % 2 == 0 ? value / 2 : 3 * value + 1) {
		collatzList.push_back(value);
	}
	collatzList.push_back(1);
	return collatzList;
}
