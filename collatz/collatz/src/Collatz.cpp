#include "Collatz.h"
#include "CollatzDelegate.h"

const CollatzDelegate delegate;

std::list<long> Collatz::getSecuence(const long value) const {
	return delegate.getSecuence(value);
}
