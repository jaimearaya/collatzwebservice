// Marks this file to host all the implementation sections of the Catch header
#define CATCH_CONFIG_RUNNER

// Makes the output for console to fit nicely within the specified number of characters
#define CATCH_CONFIG_CONSOLE_WIDTH 80

#include <catch.hpp>


/*
 * Catch provides his own implementation for main(), including the processing of the
 * command line to support a wide set of options; run it with -h or --help for details.
 */
int
main(int argc, char** argv) {
    return Catch::Session().run(argc, argv);
}
