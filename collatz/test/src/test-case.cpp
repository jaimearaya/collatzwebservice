#include "CollatzDelegate.h"
#include <catch.hpp>
#include <iostream>

TEST_CASE("Zero case") {
	std::cout << "Testing zero case" << std::endl;
	const CollatzDelegate delegate;
	CHECK_THROWS(delegate.getSecuence(0L));
}


TEST_CASE("Negative case") {
	std::cout << "Testing negative case" << std::endl;
	const CollatzDelegate delegate;
	CHECK_THROWS(delegate.getSecuence(-10L));
}


TEST_CASE("Case 1") {
	std::cout << "Testing for 1" << std::endl;
	const CollatzDelegate delegate;
	const std::list<long> result = {1L};
    CHECK(result == delegate.getSecuence(1L));
}


TEST_CASE("Case 10") {
	std::cout << "Testing for 10" << std::endl;
	const CollatzDelegate delegate;
	const std::list<long> result = {10L, 5L, 16L, 8L, 4L, 2L, 1L};
    CHECK(result == delegate.getSecuence(10L));
}
